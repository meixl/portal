/**
 * 您的公司.com
 * Copyright (c) 2018-2018 All Rights Reserved.
 */
package com.meixiaoliang.test.init;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.meixiaoliang.portal.core.util.EncryptUtil;
import com.meixiaoliang.portal.modular.system.model.User;
import com.meixiaoliang.portal.modular.system.model.constant.Constant;
import com.meixiaoliang.portal.modular.system.service.UserService;

/**
 * 用户初始化
 *
 * @author mei.xiaoliang@qq.com
 */
public class UserInit extends BaseTest {

    @Autowired
    private UserService userService;

    /** 密码加密的盐 */
    @Value("${password.salt}")
    public String       salt;
    /** 密码转换次数 */
    @Value("${password.transTime}")
    public int          transTime;

    /**
     * 重置所有密码为 123456
     */
    @Test
    //    @Ignore
    public void resetAllPwd() {

        // 批量设置默认密码
        String defaultPwd = EncryptUtil.transform(Constant.DEFAULT_PWD, salt, transTime);
        LambdaUpdateWrapper<User> updateWrapper = new UpdateWrapper<User>().lambda().set(User::getPassword, defaultPwd);
        userService.update(new User(), updateWrapper);

    }

}
