package com.meixiaoliang.portal.core.configuration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 系统拦截器注册
 *
 * @author mei.xiaoliang@qq.com
 */
@Configuration
public class SystemConfig implements WebMvcConfigurer {

    /**
     * LocalDate 入参时间转换
     */
    @Bean
    public Converter<String, LocalDate> DateConvert() {
        return new Converter<String, LocalDate>() {
            @Override
            public LocalDate convert(String source) {

                // 空入参, 不格式化
                if (StringUtils.isBlank(source)) {
                    return null;
                }

                return LocalDate.parse(source.trim(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            }
        };
    }

    /**
     * LocalDate 入参时间转换
     */
    @Bean
    public Converter<String, LocalDateTime> DateTimeConvert() {
        return new Converter<String, LocalDateTime>() {
            @Override
            public LocalDateTime convert(String source) {

                // 空入参, 不格式化
                if (StringUtils.isBlank(source)) {
                    return null;
                }

                return LocalDateTime.parse(source.trim(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
            }
        };
    }

}