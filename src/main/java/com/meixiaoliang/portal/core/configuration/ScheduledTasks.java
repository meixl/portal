package com.meixiaoliang.portal.core.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 定时任务
 */
@Configuration
public class ScheduledTasks {

    // 每月企业巡查重置
    //    @Scheduled(cron = "0/1 * * * * ?") // 每秒 1 次
    // 每月 1 号 0 时重置密码
    @Scheduled(cron = "0 0 0 1 * ? ")
    public void resetCompanyMonthTime() {
        System.out.println(123);
    }

}