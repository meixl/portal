
package com.meixiaoliang.portal.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * excel 导出列
 *
 * @author mei.xiaoliang@qq.com
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelTitleList {

    ExcelTitle[] value();

}