
package com.meixiaoliang.portal.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.meixiaoliang.portal.modular.system.model.enums.ExcelTypeEnum;

/**
 * excel 导出列
 *
 * @author mei.xiaoliang@qq.com
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ExcelTitleList.class)
public @interface ExcelTitle {

    /**
     * excel 中标题栏名称
     */
    String value() default "";

    /**
     * excel 中标题栏顺序
     * 0 为第一位
     */
    int order();

    /**
     * excel 类型
     */
    ExcelTypeEnum type();

}