
package com.meixiaoliang.portal.core.util;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.meixiaoliang.portal.core.exception.BusinessException;

/**
 * 断言工具类
 *
 * @author mei.xiaoliang@qq.com
 */
public class AssertUtil {

    /**
     * 对象空断言
     *
     * @param obj     判断对象
     * @param message 错误信息
     */
    public static void notNull(Object obj, String message) {
        if (null == obj) {
            throw new BusinessException(message);
        }
    }

    /**
     * 字符串空断言
     *
     * @param string  判断对象
     * @param message 错误信息
     */
    public static void notBlank(String string, String message) {
        if (StringUtils.isBlank(string)) {
            throw new BusinessException(message);
        }
    }

    /**
     * 集合包含元素断言
     *
     * @param list    入参集合
     * @param t       入参对象
     * @param message 错误信息
     */
    public static <T> void contains(List<T> list, T t, String message) {
        if (!list.contains(t)) {
            throw new BusinessException(message);
        }
    }

    /**
     * Integer 数字比较断言
     *
     * @param a       比较数字 1
     * @param b       比较数字 2
     * @param message 错误信息
     */
    public static void equals(Integer a, Integer b, String message) {
        notNull(a, message);
        notNull(b, message);
        if (a.intValue() != b.intValue()) {
            throw new BusinessException(message);
        }
    }

    /**
     * 判断 true 断言
     *
     * @param check   判断
     * @param message 错误信息
     */
    public static void isTrue(boolean check, String message) {
        if (!check) {
            throw new BusinessException(message);
        }
    }

    /**
     * 判断 false 断言
     *
     * @param check   判断
     * @param message 错误信息
     */
    public static void isFalse(boolean check, String message) {
        if (check) {
            throw new BusinessException(message);
        }
    }

}