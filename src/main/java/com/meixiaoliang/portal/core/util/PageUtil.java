
package com.meixiaoliang.portal.core.util;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.meixiaoliang.portal.modular.system.model.Menu;
import com.meixiaoliang.portal.modular.system.model.searcher.BaseSearcher;
import com.meixiaoliang.portal.modular.system.model.searcher.HolidaySpecialSearcher;

/**
 * 分页入参工具类
 *
 * @author mei.xiaoliang@qq.com
 */
public class PageUtil {

    /**
     * 构建分页入参, 入参必须为 BaseSearcher 的子类
     */
    public static <T> Page<T> build(BaseSearcher searcher) {
        Page<T> page = new Page<>(searcher.getPageNum(), searcher.getPageSize());
        return page;
    }

    /**
     * 构建分页入参, 入参必须为 BaseSearcher 的子类
     *
     * @param pageNum  当前分页页数
     * @param pageSize 分页每页显示条数
     * @return
     */
    public static <T> Page<T> build(long pageNum, long pageSize) {
        Page<T> page = new Page<>(pageNum, pageSize);
        return page;
    }

    /**
     * 测试
     */
    public static void main(String[] args) {
        HolidaySpecialSearcher searcher = new HolidaySpecialSearcher();
        searcher.setPageNum(2);
        searcher.setPageSize(20);
        Page<Menu> page = build(searcher);
        System.out.println(JSON.toJSONString(page));
    }

}