/**
 * 您的公司.com
 * Copyright (c) 2018-2018 All Rights Reserved.
 */
package com.meixiaoliang.portal.core.util;

import java.util.UUID;

/**
 * uuid 随机码工具
 *
 * @author mei.xiaoliang@qq.com
 */
public class UuidUtil {

    public static void main(String[] args) {
        generate();
    }

    /**
     * 生成随机 32 位
     */
    public static String generate() {
        return UUID.randomUUID().toString().replace("-", "");
    }

}