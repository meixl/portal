package com.meixiaoliang.portal.core.util;

import java.text.NumberFormat;

/**
 * 数字工具类
 */
public class NumberUtil {

    /**
     * 获取 2 个数相除百分比
     *
     * @param first  除数
     * @param second 被除数
     * @return 商（百分比）
     */
    public static String getRate(int first, int second) {

        if (0 == second) {
            return 0 + " %";
        }

        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(2);
        String result = numberFormat.format((float) first / (float) second * 100);
        return result + " %";

    }

}