
package com.meixiaoliang.portal.core.util;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import com.meixiaoliang.portal.core.exception.RedisException;

/**
 * redis 操作工具
 *
 * @author mei.xiaoliang@qq.com
 */
@Configuration
public class RedisUtil {

    private Logger              logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 放值
     */
    public void set(String key, String value) {
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        try {
            ops.set(key, value);
            logger.debug("redis 放值 key: {}, value: {}", key, value);
        } catch (Exception e) {
            throw new RedisException();
        }
    }

    /**
     * 放值（含有效期）, 过期时间：秒
     */
    public void set(String key, String value, long timeout) {
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        try {
            ops.set(key, value, timeout, TimeUnit.SECONDS);
            logger.debug("redis 放值 key: {}, value: {}, timeout: {}", key, value, timeout);
        } catch (Exception e) {
            throw new RedisException();
        }
    }

    /**
     * 取值
     */
    public String get(String key) {
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        try {
            String result = ops.get(key);
            logger.debug("redis 取值 key: {}, value: {}", key, result);
            return result;
        } catch (Exception e) {
            throw new RedisException();
        }
    }

    /**
     * 设置过期时间, 过期时间：秒
     */
    public void expire(String key, long timeout) {
        try {
            stringRedisTemplate.expire(key, timeout, TimeUnit.SECONDS);
            logger.debug("redis 设置过期时间 (单位: 秒) key:{}, timeout:{}", key, timeout);
        } catch (Exception e) {
            throw new RedisException();
        }
    }

    /**
     * 删除 key 值
     */
    public void delete(String key) {
        try {
            stringRedisTemplate.delete(key);
            logger.debug("redis 删除 key: {}", key);
        } catch (Exception e) {
            throw new RedisException();
        }
    }

}