package com.meixiaoliang.portal.core.rongyun;

/**
 * 融云服务
 */
public interface RongyunService {

    /**
     * 获取融云用户 token
     */
    String getToken(String userId, String userName);

}
