package com.meixiaoliang.portal.core.rongyun.result;

import lombok.Data;

/**
 * 红点消息：红点是否显示+其余点数量
 */
@Data
public class BellRedResult {

    /**
     * 红点是否隐藏
     */
    private boolean bellRedHidden;

    /**
     * 未处理数量
     */
    private Integer pendingNum;

}
