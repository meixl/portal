
package com.meixiaoliang.portal.core.handler;

import java.net.SocketTimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.meixiaoliang.portal.modular.system.model.response.BaseResponse;

import redis.clients.jedis.exceptions.JedisConnectionException;

/**
 * 全局异常捕获
 *
 * @author mei.xiaoliang@qq.com
 */
@ControllerAdvice
public class RedisExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 自定义异常捕获
     */
    @ExceptionHandler({ JedisConnectionException.class, SocketTimeoutException.class })
    @ResponseBody
    public String handleException() {

        logger.info("捕获到异常: code=4, msg=redis连接失败");

        BaseResponse response = new BaseResponse();
        response.setCode(4);
        response.setMsg("redis 连接失败");

        return JSON.toJSONString(response);

    }

}
