
package com.meixiaoliang.portal.core.handler;

import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.meixiaoliang.portal.modular.system.model.response.BaseResponse;

/**
 * 全局异常捕获
 *
 * @author mei.xiaoliang@qq.com
 */
@ControllerAdvice
public class ShiroExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 自定义异常捕获
     */
    @ExceptionHandler(UnauthorizedException.class)
    @ResponseBody
    public String handleException() {

        logger.info("捕获到异常: code=5, msg=用户无该权限");

        BaseResponse response = new BaseResponse();
        response.setCode(5);
        response.setMsg("用户无该权限");

        return JSON.toJSONString(response);

    }

}