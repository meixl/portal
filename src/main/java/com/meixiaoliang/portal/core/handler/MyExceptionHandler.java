
package com.meixiaoliang.portal.core.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.meixiaoliang.portal.core.exception.BaseException;
import com.meixiaoliang.portal.core.exception.BusinessException;
import com.meixiaoliang.portal.core.exception.RedisException;
import com.meixiaoliang.portal.core.exception.ValidateException;
import com.meixiaoliang.portal.modular.system.model.response.BaseResponse;

/**
 * 全局异常捕获
 *
 * @author mei.xiaoliang@qq.com
 */
@ControllerAdvice
public class MyExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 自定义异常捕获
     */
    @ExceptionHandler({ RedisException.class, ValidateException.class, BusinessException.class })
    @ResponseBody
    public String handleException(BaseException e) {

        logger.info("捕获到异常: code={}, msg={}", e.getCode(), e.getMsg());

        BaseResponse response = new BaseResponse();
        response.setCode(e.getCode());
        response.setMsg(e.getMsg());

        return JSON.toJSONString(response);

    }

}