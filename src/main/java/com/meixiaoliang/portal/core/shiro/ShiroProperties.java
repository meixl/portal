package com.meixiaoliang.portal.core.shiro;

import lombok.Data;

@Data
public class ShiroProperties {

    /**
     * session 超时时间，默认 3600000 毫秒 ———— 1 小时
     */
    public static final long   SESSION_TIMEOUT = 3600000 * 24 * 3;

    /**
     * rememberMe 有效时长，默认为 86400 秒，即一天
     */
    public static final int    COOKIE_TIMEOUT  = 86400;

    /**
     * cookie 密钥
     */
    public static final String BASE64_ENCODED  = "4AvVhjFLUs0KTA3Karstag==";

}
