
package com.meixiaoliang.portal.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * redis 数据库异常
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RedisException extends BaseException {

    /**  */
    private static final long serialVersionUID = -3521566416023870776L;

    private int               code             = 4;

    String                    msg              = "redis 数据库错误";

}