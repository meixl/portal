
package com.meixiaoliang.portal.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务系统已知异常
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseException extends RuntimeException {

    /**  */
    private static final long serialVersionUID = 2421274694789092462L;

    /** 错误码 */
    protected int             code;

    /** 错误信息 */
    protected String          msg;

}