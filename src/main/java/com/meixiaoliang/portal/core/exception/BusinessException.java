
package com.meixiaoliang.portal.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务系统已知异常
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BusinessException extends BaseException {

    /**  */
    private static final long serialVersionUID = -8780506125213515825L;

    /** 错误码: 5 */
    int                       code             = 3;

    String                    msg;

    public BusinessException(String msg) {
        super();
        this.msg = msg;
    }

}