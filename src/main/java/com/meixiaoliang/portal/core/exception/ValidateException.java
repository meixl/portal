
package com.meixiaoliang.portal.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 入参错误
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ValidateException extends BaseException {

    /**  */
    private static final long serialVersionUID = 8540991829558961829L;

    private int               code             = 5;

    String                    msg;

    public ValidateException(String msg) {
        super();
        this.msg = msg;
    }

}