
package com.meixiaoliang.portal.modular.system.model.vo;

import java.io.Serializable;

import lombok.Data;

/**
 *
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
public class DictionaryVo implements Serializable {

    /**  */
    private static final long serialVersionUID = 8227558692937391779L;

    /** 字典 key */
    private Integer           code;

    /** 字典 vlue */
    private String            value;

    /** 上级 id */
    private Integer           parent;

}