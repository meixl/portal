package com.meixiaoliang.portal.modular.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meixiaoliang.portal.modular.system.model.Dictionary;

/**
 * 数据字典
 *
 * @author mei.xiaoliang@qq.com
 */
public interface DictionaryMapper extends BaseMapper<Dictionary> {

    //    List<Dictionary> list(DictionarySearcher searcher);
    //
    //    List<DictionaryVo> listGroup(@Param("groupIds") int[] groupIds, @Param("type") int type);
    //
    //    Dictionary get();
    //
    //    /**
    //     * 字典值翻译
    //     * 
    //     * @param dictGroup 字典组
    //     * @param code      字典编码
    //     * @param type      字典类型
    //     * @return
    //     */
    //    String getDictValue(@Param("dictGroup") int dictGroup, @Param("code") int code, @Param("type") int type);
    //
    //    int add(Dictionary dictionary);
    //
    //    int update(Dictionary dictionary);
    //
    //    int delete();

}