
package com.meixiaoliang.portal.modular.system.model.constant;

/**
 * redis key 常量
 *
 * @author mei.xiaoliang@qq.com
 */
public class RedisConstant {

    /** PC session 前缀 */
    public static final String SESSION_PREFIX     = "renovate:pc:session:";

    /** shiro 缓存 */
    public static final String SHIRO_CACHE_PREFIX = "renovate:shiro:cache:";

}