package com.meixiaoliang.portal.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meixiaoliang.portal.modular.system.model.User;
import com.meixiaoliang.portal.modular.system.model.enums.BooleanEnum;

public interface UserService extends IService<User> {

    /**
     * 展示机构下的人员
     */
    List<User> listByDept(Integer deptId);

    /**
     * 用户 id 找 角色
     */
    List<Integer> listRoleById(Integer userId);

    /**
     * 用户角色找用户
     *
     * @param roleList 角色集合
     */
    List<User> listByRoles(List<Integer> roleList);

    /**
     * 用户名获取信息（登录用）
     */
    User getByName(String name);

    /**
     * 用户 id 找 name ( 缓存 )
     */
    String getNameCacheById(Integer userId);

    /**
     *
     */
    String getNamesCacheByIds(String ids);

    /**
     * 用户 id 找 name、mobile ( 缓存 )
     */
    String getNameMobileCacheById(Integer userId);

    /** 用户 id 找用户 */
    User getByUserId(int userId);

    /**
     * 密码修改
     */
    int updatePwd(int userId, String pwd);

    /**
     * 修改用户锁定状态
     */
    int updateLock(int userId, BooleanEnum type);

    void refreshCache();

}