package com.meixiaoliang.portal.modular.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.meixiaoliang.portal.modular.system.dao.DictionaryMapper;
import com.meixiaoliang.portal.modular.system.model.Dictionary;
import com.meixiaoliang.portal.modular.system.model.enums.DictionaryGroupEnum;
import com.meixiaoliang.portal.modular.system.service.DictionaryService;

/**
 * 数据字典
 *
 * @author mei.xiaoliang@qq.com
 */
@Service
public class DictionaryServiceImpl extends ServiceImpl<DictionaryMapper, Dictionary> implements DictionaryService {

    @Autowired
    private DictionaryMapper dictionaryMapper;

    /**
     * @see DictionaryService#listGroup(java.lang.Integer)
     */
    @Override
    @Cacheable("dictGroupCache")
    public List<Dictionary> listGroup(Integer groupId) {
        QueryWrapper<Dictionary> queryWrapper = new QueryWrapper<Dictionary>().eq("parent", groupId);
        return dictionaryMapper.selectList(queryWrapper);
    }

    @Override
    @Cacheable("dictGroupEnumCache")
    public List<Dictionary> listGroupByEnum(DictionaryGroupEnum groupEnum) {
        return listGroup(groupEnum.getCode());
    }

    @Override
    public List<String> listStringByEnum(DictionaryGroupEnum groupEnum) {
        List<String> list = new ArrayList<>();
        List<Dictionary> dictList = listGroupByEnum(groupEnum);
        for (Dictionary dictionary : dictList) {
            list.add(dictionary.getValue());
        }
        return list;
    }

    /**
     * @see DictionaryService#getDictValue(DictionaryGroupEnum, java.lang.Integer)
     */
    @Override
    @Cacheable("dictValue")
    public String getDictValue(DictionaryGroupEnum dictGroup, Integer code) {
        if (null == code) {
            return null;
        }
        QueryWrapper<Dictionary> queryWrapper = new QueryWrapper<Dictionary>().eq("parent", dictGroup.getCode()).eq("code", code);
        Dictionary dictionary = dictionaryMapper.selectOne(queryWrapper);
        if (null == dictionary) {
            return String.valueOf(code);
        }
        return dictionary.getValue();
    }

}
