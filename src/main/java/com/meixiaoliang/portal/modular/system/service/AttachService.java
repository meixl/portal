
package com.meixiaoliang.portal.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meixiaoliang.portal.modular.system.model.Attach;

/**
 * 附件
 *
 * @author mei.xiaoliang@qq.com
 */
public interface AttachService extends IService<Attach> {

    /**
     * 任务找附件
     */
    List<Attach> listByTask(int taskId);

    /**
     * 任务历史找附件
     */
    List<Attach> listByHistory(int historyId);

}