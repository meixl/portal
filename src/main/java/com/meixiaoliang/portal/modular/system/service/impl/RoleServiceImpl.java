package com.meixiaoliang.portal.modular.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.meixiaoliang.portal.modular.system.dao.RoleMapper;
import com.meixiaoliang.portal.modular.system.model.Role;
import com.meixiaoliang.portal.modular.system.service.RoleService;

/**
 * 角色
 *
 * @author mei.xiaoliang@qq.com
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<Integer> listIdByUserId(int userId) {
        return roleMapper.listRoleIdById(userId);
    }

    @Override
    public List<String> listCodeByUserId(int userId) {
        return roleMapper.listRoleCodeById(userId);
    }

    @Override
    public List<String> getRolesByUserId(int userId) {
        List<String> list = roleMapper.listRoleNameById(userId);
        return list;
    }

    //    @Autowired
    //    private RoleMenuDao roleMenuDao;

    //    /**
    //     * @see com.meixiaoliang.service.RoleService#list()
    //     */
    //    @Override
    //    public List<Role> list() {
    //        //        return roleMapper.list();
    //        return null;
    //    }
    //
    //    /**
    //     * @see com.meixiaoliang.service.RoleService#insert(com.meixiaoliang.entity.Role)
    //     */
    //    @Override
    //    public int insert(Role role) {
    //        return roleMapper.insert(role);
    //    }
    //
    //    /**
    //     * @see com.meixiaoliang.service.RoleService#update(com.meixiaoliang.entity.Role)
    //     */
    //    @Override
    //    public int update(Role role) {
    //        //        return roleMapper.update(role);
    //        return 1;
    //    }
    //
    //    /**
    //     * @see com.meixiaoliang.service.RoleService#delete(int)
    //     */
    //    @Override
    //    public int delete(int roleId) {
    //        //        return roleMapper.delete(roleId);
    //        return 1;
    //    }

}
