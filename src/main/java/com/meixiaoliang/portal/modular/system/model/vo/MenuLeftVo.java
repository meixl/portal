
package com.meixiaoliang.portal.modular.system.model.vo;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 登录后左边菜单数据结构显示
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
@Accessors(chain = true)
public class MenuLeftVo implements Serializable {

    /**  */
    private static final long serialVersionUID = 2557805814330566551L;

    private Integer           id;

    private String            name;

    private String            url;

    private String            icon;

    private List<MenuLeftVo>  children         = null;

}
