
package com.meixiaoliang.portal.modular.system.model;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 菜单
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
@TableName(value = "sys_menu")
@Accessors(chain = true)
public class Menu implements Serializable, Comparable<Menu> {

    /**  */
    private static final long serialVersionUID = -7228413938546153720L;

    /** 主键 */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer           id;

    /** 菜单编码（权限用） */
    private String            code;

    /** 名称 */
    private String            name;

    /** 菜单类型（1、文件夹 2、页面 3、按钮） */
    private Integer           type;

    @TableField(exist = false)
    private String            typeValue;

    /** 上级节点 */
    private Integer           parent;

    /** 标志符号 */
    private String            icon;

    /** 请求 url */
    private String            url;

    /** 顺序 */
    private Integer           num;

    /** 状态：1:启用   0:不启用  */
    private Integer           status;

    /**
     * 启用
     */
    @TableField(exist = false)
    private Boolean           statusBoolean;

    @TableField(exist = false)
    private List<Menu>        children         = null;

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Menu menu) {
        return this.num - menu.num;
    }

}
