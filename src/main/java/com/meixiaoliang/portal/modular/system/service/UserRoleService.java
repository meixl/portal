package com.meixiaoliang.portal.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meixiaoliang.portal.modular.system.model.UserRole;

/**
 * 用户-角色关系
 *
 * @author meixiaoliang@qq.com
 */
public interface UserRoleService extends IService<UserRole> {

    /**
     * 展示该用户所有角色
     */
    List<Integer> listByUserId(int userId);

    /**
     * 用户角色变更
     */
    void updateByUserId(Integer userId, List<Integer> idList);

}
