package com.meixiaoliang.portal.modular.system.model.enums;

/**
 * 菜单类型
 *
 * @author mei.xiaoliang@qq.com
 */
public enum MenuTypeEnum {

    /** 1 - 文件夹 */
    //    FOLDER(1),

    /** 2 - 页面 */
    PAGE(2, "页面"),

    /** 3 - 权限 */
    BUTTON(3, "权限"),

    ;

    private int    code;

    private String name;

    MenuTypeEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }

    /**
     * 数字获得枚举项
     *
     * @param code
     * @return
     */
    public static MenuTypeEnum getByCode(int code) {
        MenuTypeEnum result = null;
        for (MenuTypeEnum e : MenuTypeEnum.values()) {
            if (e.code == code) {
                result = e;
                break;
            }
        }
        return result;
    }

}
