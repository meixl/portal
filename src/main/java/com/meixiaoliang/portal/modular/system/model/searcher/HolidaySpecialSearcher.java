
package com.meixiaoliang.portal.modular.system.model.searcher;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 特殊节假日分页
 *
 * @author mei.xiaoliang@qq.com
 */
@Getter
@Setter
@ToString
public class HolidaySpecialSearcher extends BaseSearcher {

    /**  */
    private static final long serialVersionUID = 3770476570626648976L;

    /** 是否工作 */
    private Integer           work;

    /** 年份 */
    private Integer           year;

}