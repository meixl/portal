package com.meixiaoliang.portal.modular.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.meixiaoliang.portal.modular.system.dao.UserRoleMapper;
import com.meixiaoliang.portal.modular.system.model.UserRole;
import com.meixiaoliang.portal.modular.system.service.UserRoleService;

/**
 * 用户-角色关系
 *
 * @author meixiaoliang@qq.com
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    /**
     * @see UserRoleService#listByUserId(int)
     */
    @Override
    public List<Integer> listByUserId(int userId) {
        return userRoleMapper.listByUserId(userId);
    }

    @Override
    public void updateByUserId(Integer userId, List<Integer> idList) {
        // 删除数据
        QueryWrapper<UserRole> queryWrapper = new QueryWrapper<UserRole>().eq("user_id", userId);
        remove(queryWrapper);
        // 插入新数据
        for (Integer roleId : idList) {
            save(new UserRole().setUserId(userId).setRoleId(roleId));
        }
    }

}