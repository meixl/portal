
package com.meixiaoliang.portal.modular.system.model.enums;

/**
 *
 *
 * @author mei.xiaoliang@qq.com
 */
public enum BooleanEnum {

    /** 0-false */
    FALSE(0),

    /** 1-true */
    TRUE(1),

    ;

    private int code;

    BooleanEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    /**
     * 数字获得枚举项
     *
     * @param code
     * @return
     */
    public static BooleanEnum getByCode(int code) {
        BooleanEnum result = null;
        for (BooleanEnum e : BooleanEnum.values()) {
            if (e.code == code) {
                result = e;
                break;
            }
        }
        return result;
    }

}