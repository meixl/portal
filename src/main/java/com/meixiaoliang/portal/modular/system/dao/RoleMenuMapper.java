package com.meixiaoliang.portal.modular.system.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meixiaoliang.portal.modular.system.model.RoleMenu;

/**
 * 角色-菜单关系
 *
 * @author mei.xiaoliang@qq.com
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

    /**
     * 角色拥有菜单
     */
    @Select("select menu_id from sys_role_menu where role_id = #{roleId}")
    List<Integer> list(int roleId);

}