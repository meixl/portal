package com.meixiaoliang.portal.modular.system.model.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class BaseResultResponse<T> extends BaseResponse {

    private static final long serialVersionUID = -4890043339518622385L;

    private T                 data;

}