
package com.meixiaoliang.portal.modular.system.model;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.Data;

/**
 * 特殊节假日
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
public class HolidaySpecial implements Serializable {

    /**  */
    private static final long serialVersionUID = 3644833240837930360L;

    /** 主键 */
    private Integer           id;

    /** 日期 */
    private LocalDate         date;

    /** 是否工作 */
    private Integer           work;

    /** 是否工作展示 */
    private String            workValue;

}