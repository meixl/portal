package com.meixiaoliang.portal.modular.system.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meixiaoliang.portal.modular.system.model.User;

/**
 * 用户
 *
 * @author mei.xiaoliang@qq.com
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 用户角色找用户
     */
    List<User> listByRoles(@Param("roleList") List<Integer> roleList);

    /** 修改密码  */
    @Update("update sys_user set password = #{password} where id = #{userId}")
    int updatePwd(@Param("userId") int userId, @Param("password") String password);

    /** 修改锁定状态 */
    @Update("update sys_user set is_lock = #{type} where id = #{userId}")
    int updateLock(@Param("userId") int userId, @Param("type") int type);

}