
package com.meixiaoliang.portal.modular.system.model.constant;

/**
 * 常量
 *
 * @author mei.xiaoliang@qq.com
 */
public class Constant {

    /** 超级管理员 id */
    public final static int    ADMIN_ID       = 1;

    /** 默认密码:123456 */
    public final static String DEFAULT_PWD    = "123456";

    /** 请求成功返回响应码: 1 */
    public final static int    SUCCESS_CODE   = 1;

    /** 请求成功返回提示文字: 请求成功 */
    public final static String SUCCESS_MSG    = "操作成功";

    /** 请求未登录状态码: 2 */
    public final static int    NOT_LOGIN_CODE = 2;

    /** 请求成功返回提示文字: 请求成功 */
    public final static String NOT_LOGIN_MSG  = "对不起，您未登录或登录已过期";

    /** 批量处理个数：600 */
    public final static int    BATCH_NUM      = 600;

    /** 根节点 */
    public static final int    ROOT_POINT     = 0;

}
