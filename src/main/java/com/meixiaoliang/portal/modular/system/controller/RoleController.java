package com.meixiaoliang.portal.modular.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.meixiaoliang.portal.core.util.AssertUtil;
import com.meixiaoliang.portal.core.util.PageUtil;
import com.meixiaoliang.portal.core.util.ResponseUtil;
import com.meixiaoliang.portal.modular.system.model.Role;
import com.meixiaoliang.portal.modular.system.model.UserRole;
import com.meixiaoliang.portal.modular.system.model.searcher.BaseSearcher;
import com.meixiaoliang.portal.modular.system.service.RoleService;
import com.meixiaoliang.portal.modular.system.service.UserRoleService;

/**
 * 角色
 *
 * @author mei.xiaoliang@qq.com
 */
@RestController
@RequestMapping("/role")
public class RoleController extends BaseController {

    @Autowired
    private RoleService     roleService;
    @Autowired
    private UserRoleService userRoleService;

    /**
     * 展示所有角色
     */
    @PostMapping("/listAll")
    public String listAll() {
        LambdaQueryWrapper<Role> queryWrapper = new QueryWrapper<Role>().lambda().orderByAsc(Role::getNum);
        List<Role> list = roleService.list(queryWrapper);
        return ResponseUtil.buildSuccessResponse(list);
    }

    @PostMapping("/list")
    public String list(BaseSearcher searcher) {
        IPage<Role> iPage = roleService.page(PageUtil.build(searcher), new QueryWrapper<Role>().lambda().orderByAsc(Role::getNum));
        return ResponseUtil.buildSuccessPageResponse(iPage);
    }

    @RequestMapping("/get")
    public String get(Integer id) {
        Role role = roleService.getById(id);
        return ResponseUtil.buildSuccessResponse(role);
    }

    //    @RequiresPermissions("system:role")
    @PostMapping("/save")
    public String save(Role role) {
        roleService.save(role);
        return ResponseUtil.buildSuccessResponse();
    }

    //    @RequiresPermissions("system:role")
    @PostMapping("/update")
    public String update(Role role) {
        roleService.updateById(role);
        return ResponseUtil.buildSuccessResponse();
    }

    //    @RequiresPermissions("system:role")
    @PostMapping("/delete")
    public String delete(Integer id) {
        // 校验
        List<UserRole> userRoleList = userRoleService.list(new QueryWrapper<UserRole>().lambda().eq(UserRole::getRoleId, id));
        AssertUtil.isTrue(userRoleList.size() == 0, "存在该角色用户, 暂无法删除");
        // 删除
        roleService.removeById(id);
        return ResponseUtil.buildSuccessResponse();
    }

    /**
     * 用户 id 获取该用户所有角色 id
     */
    //    @RequiresPermissions("system:user")
    @PostMapping("/listByUserId")
    public String listByUserId(int userId) {
        List<Integer> list = roleService.listIdByUserId(userId);
        return ResponseUtil.buildSuccessResponse(list);
    }

    /**
     * 用户角色变更
     */
    //    @RequiresPermissions("system:user")
    @PostMapping("/updateByUserId")
    public String updateByUserId(Integer userId, @RequestParam("idList") List<Integer> idList) {
        userRoleService.updateByUserId(userId, idList);
        return ResponseUtil.buildSuccessResponse();
    }

}
