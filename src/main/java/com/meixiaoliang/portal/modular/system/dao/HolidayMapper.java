package com.meixiaoliang.portal.modular.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meixiaoliang.portal.modular.system.model.Holiday;

/**
 * 普通节假日
 *
 * @author mei.xiaoliang@qq.com
 */
public interface HolidayMapper extends BaseMapper<Holiday> {

}