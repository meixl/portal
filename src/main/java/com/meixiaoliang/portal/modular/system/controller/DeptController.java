package com.meixiaoliang.portal.modular.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.meixiaoliang.portal.core.util.ResponseUtil;
import com.meixiaoliang.portal.modular.system.model.Dept;
import com.meixiaoliang.portal.modular.system.service.DeptService;

/**
 * 部门控制器
 *
 * @author fengshuonan
 * @Date 2017年2月17日20:27:22
 */
@RestController
@RequestMapping("/dept")
public class DeptController extends BaseController {

    @Autowired
    private DeptService deptService;

    /**
     * 跳转到部门管理首页
     */
    @GetMapping("/list")
    public ModelAndView list(ModelAndView mov) {
        mov.setViewName("system/deptList");
        return mov;
    }

    /**
     * 获取所有部门, 前端拼接树结构
     */
    @PostMapping("/list")
    public String list() {
        List<Dept> list = deptService.list(null);
        return ResponseUtil.buildSuccessResponse(list);
    }

    /**
     * 机构末级单选(radio)页面
     */
    @RequestMapping("/listRadio")
    public ModelAndView listRadio(ModelAndView mov) {
        mov.setViewName("system/deptRadio");
        return mov;
    }

    /**
     * 获得部门下所有部门
     */
    @PostMapping("/listById")
    public String listById(Integer deptId) {
        List<Dept> list = deptService.listById(deptId);
        return ResponseUtil.buildSuccessResponse(list);
    }

    /**
     * 部门详情
     */
    @PostMapping("/get")
    public String get(Integer id) {
        return ResponseUtil.buildSuccessResponse(deptService.getById(id));
    }

    /**
     * 新增部门
     */
    @PostMapping("/insert")
    public String insert(Dept dept) {
        deptService.save(dept);
        return ResponseUtil.buildSuccessResponse();
    }

    /**
     * 修改部门
     */
    @PostMapping("/update")
    public String update(Dept dept) {
        deptService.updateById(dept);
        return ResponseUtil.buildSuccessResponse();
    }

    /**
     * 删除部门
     */
    @RequestMapping("/delete")
    public String delete(Integer id) {
        deptService.removeById(id);
        return ResponseUtil.buildSuccessResponse();
    }

}