
package com.meixiaoliang.portal.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * 角色
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
@TableName(value = "sys_role")
public class Role implements Serializable {

    /**  */
    private static final long serialVersionUID = -7549252920283647464L;

    /** 主键 */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer           id;

    /** 角色编码 */
    private String            code;

    /** 角色名称 */
    private String            name;

    /** 顺序 */
    private Integer           num;

}