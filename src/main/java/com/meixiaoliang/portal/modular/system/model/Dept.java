
package com.meixiaoliang.portal.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * 人员机构
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
@TableName(value = "sys_dept")
public class Dept implements Serializable {

    /**  */
    private static final long serialVersionUID = -5073742655634696885L;

    /** 主键 */
    private Integer           id;

    /** 机构名称 */
    private String            name;

    /** 上级机构节点 */
    private Integer           parent;

    /** 顺序 */
    private Integer           num;

}
