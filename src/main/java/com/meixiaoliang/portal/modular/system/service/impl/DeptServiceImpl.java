package com.meixiaoliang.portal.modular.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.meixiaoliang.portal.modular.system.dao.DeptMapper;
import com.meixiaoliang.portal.modular.system.model.Dept;
import com.meixiaoliang.portal.modular.system.service.DeptService;

/**
 * 机构
 *
 * @author mei.xiaoliang@qq.com
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements DeptService {

    @Autowired
    private DeptMapper deptMapper;

    /**
     * @see DeptService#listById(java.lang.Integer)
     */
    @Override
    public List<Dept> listById(Integer deptId) {
        QueryWrapper<Dept> queryWrapper = new QueryWrapper<Dept>().eq("parent", deptId);
        List<Dept> list = deptMapper.selectList(queryWrapper);
        return list;
    }

}