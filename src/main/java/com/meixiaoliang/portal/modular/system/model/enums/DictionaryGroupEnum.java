package com.meixiaoliang.portal.modular.system.model.enums;

/**
 * 数据数据字典类型 enum
 *
 * @author mei.xiaoliang@qq.com
 */
public enum DictionaryGroupEnum {

    /** 布尔：0-否 1-是 */
    BOOLEAN(1),

    /** 完成状态 */
    RESULT(2),

    /** 人员状态 */
    USER_STATUS(3),

    /** 菜单类型 */
    MENU_TYPE(4),

    /** 人员状态 */
    MONTH(5),

    ;

    private int code;

    DictionaryGroupEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

}