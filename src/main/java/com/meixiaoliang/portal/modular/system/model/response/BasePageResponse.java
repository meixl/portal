package com.meixiaoliang.portal.modular.system.model.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class BasePageResponse<T> extends BaseResponse {

    private static final long serialVersionUID = 5227095557688118751L;

    /**
     * 分页展示数据
     */
    private List<T>           list;

    /**
     * 当前分页页码
     */
    private long              pageNum;

    /**
     * 分页每页条目
     */
    private long              pageSize;

    /**
     * 分页总条数
     */
    private long              total;

    /**
     * 分页总页数
     */
    private long              pages;

}