package com.meixiaoliang.portal.modular.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meixiaoliang.portal.modular.system.model.HolidaySpecial;

/**
 * 特殊节假日
 *
 * @author mei.xiaoliang@qq.com
 */
public interface HolidaySpecialMapper extends BaseMapper<HolidaySpecial> {

}