package com.meixiaoliang.portal.modular.system.model.searcher;

import lombok.Getter;
import lombok.Setter;

/**
 * 数据字典分页-入参
 *
 * @author mei.xiaoliang@qq.com
 */
@Getter
@Setter
public class DictionarySearcher extends BaseSearcher {

    /**  */
    private static final long serialVersionUID = -3473900497623814591L;

    /** 查询条件 */
    private Integer           type;

}