package com.meixiaoliang.portal.modular.system.controller;

import java.util.Arrays;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import com.meixiaoliang.portal.modular.system.model.User;
import com.meixiaoliang.portal.modular.system.model.enums.RoleEnum;
import com.meixiaoliang.portal.modular.system.service.DictionaryService;
import com.meixiaoliang.portal.modular.system.service.UserService;

@RestController
@Transactional
public class BaseController {

    protected Logger  logger = LoggerFactory.getLogger(this.getClass());

    /** 文件下载前缀 */
    @Value("${downloadDir}")
    protected String  DOWNLOAD_DIR;

    @Autowired
    UserService       userService;
    @Autowired
    DictionaryService dictionaryService;

    Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    /**
     * 获取当前用户 id
     */
    Integer getCurrentUserId() {
        User user = (User) getSubject().getPrincipal();
        Integer userId = user.getId();
        logger.info("当前用户 id: {}, name: {}", userId, user.getRealName());
        return userId;
    }

    User getUser() {
        return (User) getSubject().getPrincipal();
    }

    /**
     * 当前用户是否领导
     */
    boolean isLeader() {
        List<Integer> currRoleList = userService.listRoleById(getCurrentUserId());
        List<Integer> roleList = Arrays.asList(RoleEnum.MAIN.getCode(), RoleEnum.MINOR.getCode());
        boolean result = false;
        for (Integer role : roleList) {
            for (Integer currRole : currRoleList) {
                if (role.intValue() == currRole.intValue()) {
                    result = true;
                    break;
                }
            }
        }
        logger.info("当前用户是否领导: {}", result);
        return result;
    }

}
