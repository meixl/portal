
package com.meixiaoliang.portal.modular.system.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * 附件
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
@TableName(value = "sys_attach")
public class Attach implements Serializable {

    /**  */
    private static final long serialVersionUID = -6730591069856586356L;

    /** 主键 */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer           id;

    /** uuid */
    private String            uuid;

    /** 名称 */
    private String            name;

    /** 路径 */
    private String            path;

    /** 大小 */
    private long              size;

    /** 尾缀 */
    private String            suffix;

    /** 上传用户 */
    private Integer           userId;

    /** 更新时间 */
    private LocalDateTime     updateTime;

}