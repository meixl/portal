
package com.meixiaoliang.portal.modular.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meixiaoliang.portal.modular.system.dao.HolidaySpecialMapper;
import com.meixiaoliang.portal.modular.system.service.HolidaySpecialService;

/**
 * 特殊节假日
 *
 * @author mei.xiaoliang@qq.com
 */
@Service
public class HolidaySpecialServiceImpl implements HolidaySpecialService {

    @SuppressWarnings("unused")
    @Autowired
    private HolidaySpecialMapper holidaySpecialMapper;

    //    /** 
    //     * @see com.meixiaoliang.service.HolidaySpecialService#list(com.meixiaoliang.entity.searcher.HolidaySpecialSearcher)
    //     */
    //    @Override
    //    public List<HolidaySpecial> list(HolidaySpecialSearcher searcher) {
    //        List<HolidaySpecial> list = holidaySpecialDao.list(searcher);
    //        return list;
    //    }
    //
    //    /** 
    //     * @see com.meixiaoliang.service.HolidaySpecialService#insert(com.meixiaoliang.entity.HolidaySpecial)
    //     */
    //    @Override
    //    public int insert(HolidaySpecial holidaySpecial) {
    //        int result = 0;
    //        if (null == holidaySpecialDao.getWork(holidaySpecial.getDate())) {
    //            result = holidaySpecialDao.insert(holidaySpecial);
    //        }
    //        return result;
    //    }
    //
    //    /** 
    //     * @see com.meixiaoliang.service.HolidaySpecialService#update(com.meixiaoliang.entity.HolidaySpecial)
    //     */
    //    @Override
    //    public int update(HolidaySpecial holidaySpecial) {
    //        return holidaySpecialDao.update(holidaySpecial);
    //    }
    //
    //    /** 
    //     * @see com.meixiaoliang.service.HolidaySpecialService#delete(Integer)
    //     */
    //    @Override
    //    public int delete(Integer id) {
    //        return holidaySpecialDao.delete(id);
    //    }

}