package com.meixiaoliang.portal.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meixiaoliang.portal.core.annotation.ExcelTitle;
import com.meixiaoliang.portal.modular.system.model.enums.ExcelTypeEnum;

import lombok.Data;

/**
 * 数据字典
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
@TableName(value = "sys_dictionary")
public class Dictionary implements Serializable {

    /**  */
    private static final long serialVersionUID = 2878716660914333829L;

    /** 主键 */
    @TableId(value = "id", type = IdType.AUTO)
    @ExcelTitle(value = "主键", order = 3, type = ExcelTypeEnum.DICT)
    private Integer           id;

    /** 字典 key */
    @ExcelTitle(value = "字典 code", order = 0, type = ExcelTypeEnum.DICT)
    private Integer           code;

    /** 字典 vlue */
    @ExcelTitle(value = "字典值", order = 1, type = ExcelTypeEnum.DICT)
    private String            value;

    /** 上级 id */
    @ExcelTitle(value = "上级", order = 2, type = ExcelTypeEnum.DICT)
    private Integer           parent;

}