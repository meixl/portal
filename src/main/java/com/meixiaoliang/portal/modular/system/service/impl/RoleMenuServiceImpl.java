
package com.meixiaoliang.portal.modular.system.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.meixiaoliang.portal.modular.system.dao.RoleMenuMapper;
import com.meixiaoliang.portal.modular.system.model.RoleMenu;
import com.meixiaoliang.portal.modular.system.service.RoleMenuService;

/**
 *
 *
 * @author mei.xiaoliang@qq.com
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

}