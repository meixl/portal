
package com.meixiaoliang.portal.modular.system.model;

import java.io.Serializable;

import lombok.Data;

/**
 * 节假日
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
public class Holiday implements Serializable {

    /**  */
    private static final long serialVersionUID = 8887638026360956498L;

    /** 主键 */
    private Integer           id;

    /** 名称 */
    private String            name;

    /** 是否工作 */
    private Integer           work;

    /** 是否工作显示 */
    private String            workValue;

}