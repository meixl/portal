
package com.meixiaoliang.portal.modular.system.model.vo;

import java.io.Serializable;

import lombok.Data;

/**
 *
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
public class MenuVo implements Serializable {

    /**  */
    private static final long serialVersionUID = 5927459833648428973L;

    /** 主键 */
    private Integer           id;

    /** 名称 */
    private String            name;

    /**  */
    private String            url;

    /** 上级节点 */
    private Integer           parent;

}