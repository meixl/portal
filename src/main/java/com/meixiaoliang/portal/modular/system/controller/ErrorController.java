package com.meixiaoliang.portal.modular.system.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/error")
public class ErrorController extends BaseController {

    @RequestMapping("/403")
    public ModelAndView page403(ModelAndView mov) {
        logger.info("用户无该权限");
        mov.setViewName("/error/403");
        return mov;
    }

}