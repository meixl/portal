package com.meixiaoliang.portal.modular.system.model.result;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UploadResult implements Serializable {

    /** 附件记录 id */
    private Integer id;

    /** 附件 uuid */
    private String  uuid;

    /** 附件名称*/
    private String  name;

    /** 下载地址 */
    private String  downloadUrl;

}
