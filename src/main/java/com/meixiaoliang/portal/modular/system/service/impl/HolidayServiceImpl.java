
package com.meixiaoliang.portal.modular.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.meixiaoliang.portal.modular.system.dao.DeptMapper;
import com.meixiaoliang.portal.modular.system.dao.HolidayMapper;
import com.meixiaoliang.portal.modular.system.dao.HolidaySpecialMapper;
import com.meixiaoliang.portal.modular.system.model.Dept;
import com.meixiaoliang.portal.modular.system.service.HolidayService;

/**
 * 普通节假日
 *
 * @author mei.xiaoliang@qq.com
 */
@Service
@SuppressWarnings("unused")
public class HolidayServiceImpl extends ServiceImpl<DeptMapper, Dept> implements HolidayService {

    @Autowired
    private HolidayMapper        holidayMapper;

    @Autowired
    private HolidaySpecialMapper holidaySpecialMapper;

    /**
     * @see com.meixiaoliang.service.HolidayService#update(int, int)
     */
    @Override
    public int update(int id, int work) {
        return 1;
        //        return holidayMapper.update(id, work);
    }

    /**
     * @see com.meixiaoliang.service.HolidayService#isHoliday()
     */
    @Override
    public boolean isHoliday() {

        //        boolean result = false;
        //
        //        Date now = new Date();
        //        Calendar cal = Calendar.getInstance();
        //        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        //        try {
        //            now = sdf.parse(sdf.format(now));
        //        } catch (ParseException e) {
        //        }
        //        cal.setTime(now);
        //        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 1;
        //
        //        // 特殊工作日, 特殊判断
        //        HolidaySpecial holidaySpecial = holidaySpecialMapper.getWork(now);
        //        if (null != holidaySpecial) {
        //            result = BooleanEnum.TRUE.getCode() == holidaySpecial.getWork().intValue();
        //        }
        //        // 普通工作日, 普通判断
        //        else {
        //            result = holidayMapper.getWork(dayOfWeek);
        //        }
        //
        //        return result;

        return false;
    }

}