package com.meixiaoliang.portal.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Data;

/**
 * 基础 pojo, 其余 pojo 请继承它
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
public class BaseModel implements Serializable {

    private static final long serialVersionUID = 3556000664114522215L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer           id;

}