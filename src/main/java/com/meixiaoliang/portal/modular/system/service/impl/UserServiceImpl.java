package com.meixiaoliang.portal.modular.system.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.meixiaoliang.portal.modular.system.dao.RoleMapper;
import com.meixiaoliang.portal.modular.system.dao.UserMapper;
import com.meixiaoliang.portal.modular.system.model.User;
import com.meixiaoliang.portal.modular.system.model.enums.BooleanEnum;
import com.meixiaoliang.portal.modular.system.service.MenuService;
import com.meixiaoliang.portal.modular.system.service.RoleService;
import com.meixiaoliang.portal.modular.system.service.UserService;

/**
 *
 *
 * @author mei.xiaoliang@qq.com
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private RoleService roleService;
    @Autowired
    private MenuService menuService;
    @Autowired
    private UserMapper  userMapper;
    @Autowired
    private RoleMapper  roleMapper;

    @Override
    public List<User> listByDept(Integer deptId) {
        return null;
    }

    /**
     * @see UserService#listRoleById(java.lang.Integer)
     */
    @Override
    public List<Integer> listRoleById(Integer userId) {
        return roleMapper.listRoleIdById(userId);
    }

    @Override
    public List<User> listByRoles(List<Integer> roleList) {
        List<User> userList = userMapper.listByRoles(roleList);
        return userList;
    }

    @Override
    public User getByName(String name) {
        LambdaQueryWrapper<User> queryWrapper = new QueryWrapper<User>().lambda();
        queryWrapper.eq(User::getName, name);
        return getOne(queryWrapper);
    }

    /**
     * @see UserService#getNameCacheById(java.lang.Integer)
     */
    @Override
    @Cacheable("userNameCache")
    public String getNameCacheById(Integer userId) {
        if (null == userId) {
            return null;
        }
        User user = getById(userId);
        return user.getRealName();
    }

    @Override
    public String getNamesCacheByIds(String ids) {
        if (StringUtils.isBlank(ids)) {
            return null;
        }

        String[] array = StringUtils.split(ids, ",");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            sb.append(getNameCacheById(Integer.valueOf(array[i])));
            if (i != array.length - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    /**
     * @see UserService#getNameMobileCacheById(java.lang.Integer)
     */
    @Override
    @Cacheable("userNameMobileCache")
    public String getNameMobileCacheById(Integer userId) {
        User user = getById(userId);
        String mobile = StringUtils.isNotBlank(user.getMobile()) ? "(" + user.getMobile() + ")" : "";
        return user.getRealName() + mobile;
    }

    @Override
    public User getByUserId(int userId) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>().select("id, real_name").eq("id", userId);
        User user = getOne(queryWrapper);
        user.setRoleList(roleService.listIdByUserId(userId));
        user.setPermissionList(menuService.listPermissionByUserId(userId));
        return user;
    }

    @Override
    public int updatePwd(int userId, String pwd) {
        return userMapper.updatePwd(userId, pwd);
    }

    @Override
    public int updateLock(int userId, BooleanEnum type) {
        return userMapper.updateLock(userId, type.getCode());
    }

    @Override
    @CacheEvict(value = { "companyName", "dictValue", "userNameCache", "userNameMobileCache", "ruleDictValue", "ruleNameCache", "roleMenuCache",
                          "taskTypeCache", "taskTypesCache", "dictGroupCache", "taskTypeCodeCache", "dictGroupEnumCache" }, allEntries = true)
    public void refreshCache() {

    }

}
