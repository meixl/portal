package com.meixiaoliang.portal.modular.system.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meixiaoliang.portal.modular.system.model.Attach;

/**
 * 附件 
 *
 * @author mei.xiaoliang@qq.com
 */
public interface AttachMapper extends BaseMapper<Attach> {

    /**
     * 任务找附件
     */
    @Select("select id, uuid, name from sys_attach left join task_attach on sys_attach.id = task_attach.attach_id "
            + "where task_attach.task_id = #{param} and is_deleted = 0")
    List<Attach> listByTask(int taskId);

    /**
     * 任务历史找附件
     */
    @Select("select id, uuid, name from sys_attach left join task_history_attach on sys_attach.id = task_history_attach.attach_id "
            + "where task_history_attach.task_history_id = #{param} and is_deleted = 0")
    List<Attach> listByHistory(int historyId);

}