package com.meixiaoliang.portal.modular.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meixiaoliang.portal.core.util.ResponseUtil;
import com.meixiaoliang.portal.modular.system.model.Dictionary;
import com.meixiaoliang.portal.modular.system.model.enums.DictionaryGroupEnum;
import com.meixiaoliang.portal.modular.system.service.DictionaryService;

/**
 * 数据字典
 *
 * @author mei.xiaoliang@qq.com
 */
@RestController
@RequestMapping("/dictionary")
@CrossOrigin
public class DictionaryController extends BaseController {

    @Autowired
    private DictionaryService dictionaryService;

    /**
     * 分页页面
     */
    @GetMapping("/list")
    public ModelAndView list(ModelAndView mov) {
        mov.setViewName("system/dictionaryList");
        return mov;
    }

    /**
     * 展示所有, 前端 ztree 构建
     */
    @PostMapping("/list")
    public String list() {
        QueryWrapper<Dictionary> queryWrapper = new QueryWrapper<Dictionary>();
        List<Dictionary> list = dictionaryService.list(queryWrapper);
        return ResponseUtil.buildSuccessResponse(list);
    }

    /**
     * 获取指定字典结构
     * 请尽可能使用 listGroupByEnum 方法
     */
    @RequestMapping("/listGroup")
    @Deprecated
    public String listGroup(Integer groupId) {
        List<Dictionary> resultList = dictionaryService.listGroup(groupId);
        return ResponseUtil.buildSuccessResponse(resultList);
    }

    /**
     * 获取指定字典结构
     */
    @RequestMapping("/listGroupByEnum")
    public String listGroupByEnum(DictionaryGroupEnum groupEnum) {
        List<Dictionary> resultList = dictionaryService.listGroupByEnum(groupEnum);
        return ResponseUtil.buildSuccessResponse(resultList);
    }

}