package com.meixiaoliang.portal.modular.system.model.enums;

/**
 * 菜单级别
 *
 * @author mei.xiaoliang@qq.com
 */
public enum MenuLevelEnum {

    /** 1 - 一级菜单 */
    FIRST(1),

    /** 2 - 二级菜单 */
    SECOND(2),

    /** 3 - 三级菜单 */
    THIRD(3),

    ;

    private int code;

    MenuLevelEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

}
