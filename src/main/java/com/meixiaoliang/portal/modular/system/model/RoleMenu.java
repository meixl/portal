
package com.meixiaoliang.portal.modular.system.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 角色-菜单关系
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
@TableName(value = "sys_role_menu")
@Accessors(chain = true)
public class RoleMenu implements Serializable {

    /**  */
    private static final long serialVersionUID = -7549252920283647464L;

    /** 角色 id */
    private Integer           roleId;

    /** 菜单 id */
    private Integer           menuId;

}