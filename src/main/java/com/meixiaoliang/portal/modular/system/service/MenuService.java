package com.meixiaoliang.portal.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meixiaoliang.portal.modular.system.model.Menu;
import com.meixiaoliang.portal.modular.system.model.result.MenuListResult;
import com.meixiaoliang.portal.modular.system.model.vo.MenuLeftVo;

/**
 * 菜单
 *
 * @author mei.xiaoliang@qq.com
 */
public interface MenuService extends IService<Menu> {

    List<Menu> listAll();

    List<MenuLeftVo> listSingleAll();

    /**
     * 显示角色左边菜单
     */
    MenuListResult listLeft(int userId);

    /**
     * 用户权限
     *
     * @param userId 用户 id
     * @return 用户权限集合
     */
    List<String> listPermissionByUserId(int userId);

}
