
package com.meixiaoliang.portal.modular.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meixiaoliang.portal.modular.system.model.RoleMenu;

/**
 * 角色
 *
 * @author mei.xiaoliang@qq.com
 */
public interface RoleMenuService extends IService<RoleMenu> {

}