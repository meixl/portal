
package com.meixiaoliang.portal.modular.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meixiaoliang.portal.modular.system.model.Dept;

/**
 * 普通节假日
 *
 * @author mei.xiaoliang@qq.com
 */
public interface HolidayService extends IService<Dept> {

    boolean isHoliday();

    int update(int id, int work);

}