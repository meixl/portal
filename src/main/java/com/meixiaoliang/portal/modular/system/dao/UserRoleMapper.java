package com.meixiaoliang.portal.modular.system.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meixiaoliang.portal.modular.system.model.UserRole;

/**
 * 用户-角色关系
 *
 * @author meixiaoliang@qq.com
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 用户 id 找角色
     */
    @Select("select role_id from sys_user_role where user_id = #{userId}")
    List<Integer> listByUserId(int userId);

}
