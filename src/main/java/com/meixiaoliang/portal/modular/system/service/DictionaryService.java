
package com.meixiaoliang.portal.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meixiaoliang.portal.modular.system.model.Dictionary;
import com.meixiaoliang.portal.modular.system.model.enums.DictionaryGroupEnum;

/**
 * 数据字典
 *
 * @author mei.xiaoliang@qq.com
 */
public interface DictionaryService extends IService<Dictionary> {

    /**
     *
     *
     * @param groupId
     * @return
     */
    List<Dictionary> listGroup(Integer groupId);

    /**
     * 枚举找数据字典列表
     *
     * @param groupEnum
     * @return
     */
    List<Dictionary> listGroupByEnum(DictionaryGroupEnum groupEnum);

    /**
     * 获取字典列表
     *
     * @param groupEnum 请求字典组枚举
     * @return 字典列表（字符串形式）
     */
    List<String> listStringByEnum(DictionaryGroupEnum groupEnum);

    /**
     * 字典翻译
     *
     * @param dictGroup 字典组
     * @param code      字典编码
     * @return
     */
    String getDictValue(DictionaryGroupEnum dictGroup, Integer code);

}