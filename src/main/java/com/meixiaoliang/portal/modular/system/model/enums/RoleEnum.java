
package com.meixiaoliang.portal.modular.system.model.enums;

/**
 *
 *
 * @author mei.xiaoliang@qq.com
 */
public enum RoleEnum {

    /** 1 - 超级管理员 */
    ADMIN(1),

    /** 2 - 镇长、书记*/
    MAIN(2),

    /** 3 - 副镇长 */
    MINOR(3),

    /** 4 - 普通用户 */
    COMMON(4),

    ;

    private int code;

    RoleEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    /**
     * 数字获得枚举项
     *
     * @param code
     * @return
     */
    public static RoleEnum getByCode(int code) {
        RoleEnum result = null;
        for (RoleEnum e : RoleEnum.values()) {
            if (e.code == code) {
                result = e;
                break;
            }
        }
        return result;
    }

}