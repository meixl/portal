package com.meixiaoliang.portal.modular.system.model.result;

import java.util.List;
import java.util.Map;

import com.meixiaoliang.portal.modular.system.model.vo.MenuLeftVo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class MenuListResult {

    /**
     * 左侧菜单
     */
    private List<MenuLeftVo>                 menuLeftVoList;

    /**
     * 路由对应面包屑
     */
    private Map<String, Map<String, Object>> menuBreadMap;

}
