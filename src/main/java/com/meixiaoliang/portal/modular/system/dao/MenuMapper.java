package com.meixiaoliang.portal.modular.system.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meixiaoliang.portal.modular.system.model.Menu;
import com.meixiaoliang.portal.modular.system.model.vo.MenuVo;

/**
 * 菜单
 *
 * @author mei.xiaoliang@qq.com
 */
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 查询所有菜单
     */
    @Select("select id, name, url, parent, icon from sys_menu")
    List<MenuVo> listAll();

    /**
     * 用户左侧菜单
     *
     * @param userId 用户 id
     * @param status 菜单状态（未删除）
     * @return 菜单
     */
    @Select("select distinct id, name, parent, icon, url, num FROM sys_menu LEFT JOIN sys_role_menu ON sys_menu.id = sys_role_menu.menu_id where role_id IN ( SELECT role_id FROM sys_user_role WHERE user_id = #{userId} and sys_menu.status = #{status} )")
    List<Menu> listLeft(@Param("userId") int userId, @Param("status") int status);

    /**
     * 用户拥有权限
     *
     * @param userId 用户 id
     * @param status 菜单状态（未删除）
     * @return 菜单
     */
    @Select("select code FROM sys_menu LEFT JOIN sys_role_menu ON sys_menu.id = sys_role_menu.menu_id where role_id IN ( SELECT role_id FROM sys_user_role WHERE user_id = #{userId} and sys_menu.status = #{status} and sys_menu.code != '' )")
    List<String> listPermission(@Param("userId") int userId, @Param("status") int status);

    @Select("SELECT CODE FROM sys_menu WHERE sys_menu.status = #{status} and sys_menu.code != ''")
    List<String> listAdminPermission(int code);

}