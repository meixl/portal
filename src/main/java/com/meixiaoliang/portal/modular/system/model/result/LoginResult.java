
package com.meixiaoliang.portal.modular.system.model.result;

import java.io.Serializable;

import com.meixiaoliang.portal.modular.system.model.User;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 登录结果
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
@Accessors(chain = true)
public class LoginResult implements Serializable {

    /**  */
    private static final long serialVersionUID = 1161042600693268396L;

    /** 登录结果 */
    private boolean           loginResult;

    /** 登录返回信息 */
    private String            message;

    /** 用户 token */
    private String            token;

    /** 用户信息 */
    private User              user;

}