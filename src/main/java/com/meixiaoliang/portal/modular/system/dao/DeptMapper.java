package com.meixiaoliang.portal.modular.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meixiaoliang.portal.modular.system.model.Dept;

/**
 * 机构
 *
 * @author mei.xiaoliang@qq.com
 */
public interface DeptMapper extends BaseMapper<Dept> {

}