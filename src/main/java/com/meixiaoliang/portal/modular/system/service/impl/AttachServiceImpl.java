package com.meixiaoliang.portal.modular.system.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.meixiaoliang.portal.modular.system.dao.AttachMapper;
import com.meixiaoliang.portal.modular.system.model.Attach;
import com.meixiaoliang.portal.modular.system.service.AttachService;

/**
 * 附件
 *
 * @author mei.xiaoliang@qq.com
 */
@Service
public class AttachServiceImpl extends ServiceImpl<AttachMapper, Attach> implements AttachService {

    @Autowired
    private AttachMapper attachMapper;

    /** 文件下载前缀 */
    @Value("${downloadDir}")
    public String        DOWNLOAD_DIR;

    /**
     * @see com.CompanyService.service.AttachService#listByTask(int)
     */
    @Override
    public List<Attach> listByTask(int taskId) {
        List<Attach> attachList = attachMapper.listByTask(taskId);
        for (Attach attach : attachList) {
            attach.setPath(DOWNLOAD_DIR + "/" + attach.getId() + "/" + attach.getUuid() + "/" + attach.getName());
        }
        return attachList;
    }

    /**
     * @see com.CompanyService.service.AttachService#listByHistory(int)
     */
    @Override
    public List<Attach> listByHistory(int historyId) {
        List<Attach> attachList = attachMapper.listByHistory(historyId);
        for (Attach attach : attachList) {
            attach.setPath(DOWNLOAD_DIR + "/" + attach.getId() + "/" + attach.getUuid() + "/" + attach.getName());
        }
        return attachList;
    }

}