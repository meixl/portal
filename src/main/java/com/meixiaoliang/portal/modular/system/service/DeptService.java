
package com.meixiaoliang.portal.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meixiaoliang.portal.modular.system.model.Dept;

/**
 * 机构
 *
 * @author mei.xiaoliang@qq.com
 */
public interface DeptService extends IService<Dept> {

    /**
     * 获得部门下所有部门
     *
     * @param deptId
     * @return
     */
    List<Dept> listById(Integer deptId);

}