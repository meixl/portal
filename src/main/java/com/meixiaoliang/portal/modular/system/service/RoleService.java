
package com.meixiaoliang.portal.modular.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meixiaoliang.portal.modular.system.model.Role;

/**
 * 角色
 *
 * @author mei.xiaoliang@qq.com
 */
public interface RoleService extends IService<Role> {

    List<Integer> listIdByUserId(int userId);

    List<String> listCodeByUserId(int userId);

    List<String> getRolesByUserId(int userId);

}
