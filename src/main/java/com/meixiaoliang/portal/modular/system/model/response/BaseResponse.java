package com.meixiaoliang.portal.modular.system.model.response;

import java.io.Serializable;

import lombok.Data;

@Data
public class BaseResponse implements Serializable {

    private static final long serialVersionUID = 3313806387086290717L;

    private int               code;

    /** 结果字符串 */
    private String            msg;

}