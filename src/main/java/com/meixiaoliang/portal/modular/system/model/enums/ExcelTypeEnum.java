
package com.meixiaoliang.portal.modular.system.model.enums;

/**
 *
 *
 * @author mei.xiaoliang@qq.com
 */
public enum ExcelTypeEnum {

    /**  */
    DICT,

    TASK,

    TASK_2_1,

    TASK_2_2,

    ;

}