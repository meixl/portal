package com.meixiaoliang.portal.modular.system.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author meixiaoliang@qq.com
 * @since 2019-01-02
 */
@Data
@Accessors(chain = true)
@TableName("sys_user_role")
public class UserRole {

    /**  */
    private static final long serialVersionUID = 8998946576402501331L;

    private Integer           userId;

    private Integer           roleId;

    @TableLogic
    @TableField(value = "is_deleted")
    private Integer           deleted;

}
