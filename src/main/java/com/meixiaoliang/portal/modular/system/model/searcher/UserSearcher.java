package com.meixiaoliang.portal.modular.system.model.searcher;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserSearcher extends BaseSearcher {

    private String name;

}