package com.meixiaoliang.portal.modular.system.model;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@TableName(value = "sys_user")
@Accessors(chain = true)
public class User implements Serializable {

    private static final long serialVersionUID = 3100577838223312938L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer           id;

    private String            name;

    private String            password;

    private String            realName;

    private String            mobile;

    private Integer           status;

    @TableField(exist = false)
    private List<Integer>     roleList;

    @TableField(exist = false)
    private List<String>      permissionList;

    @TableField(exist = false)
    private List<Integer>     departmentList;

    @TableLogic
    @TableField(value = "is_deleted")
    private Integer           deleted;

    @TableField(exist = false)
    private String            statusValue;

    /**
     * 状态布尔
     */
    @TableField(exist = false)
    private Boolean           statusBool;

    @TableField(exist = false)
    private List<String>      roles;

    /**
     * 用户融云id
     */
    @TableField(exist = false)
    private String            rongyunId;

    /**
     * 融云appKey
     */
    @TableField(exist = false)
    private String            rongyunAppKey;

    /**
     * 融云token
     */
    @TableField(exist = false)
    private String            rongyunToken;

}
