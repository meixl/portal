package com.meixiaoliang.portal.modular.system.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meixiaoliang.portal.modular.system.model.Role;

/**
 * 角色
 *
 * @author mei.xiaoliang@qq.com
 */
public interface RoleMapper extends BaseMapper<Role> {

    /**  用户 id 找角色 */
    @Select("select distinct role_id from sys_user_role where user_id = #{userId} and sys_user_role.is_deleted = 0")
    List<Integer> listRoleIdById(Integer userId);

    /**  用户 id 找角色 code */
    @Select("SELECT CODE FROM sys_role INNER JOIN sys_user_role ON sys_role.id = sys_user_role.role_id WHERE sys_user_role.user_id = #{userId} and sys_user_role.is_deleted = 0")
    List<String> listRoleCodeById(Integer userId);

    /**  用户 id 找角色 code */
    @Select("SELECT NAME FROM sys_role INNER JOIN sys_user_role ON sys_role.id = sys_user_role.role_id WHERE sys_user_role.user_id = #{userId} and sys_user_role.is_deleted = 0")
    List<String> listRoleNameById(Integer userId);

}
