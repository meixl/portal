package com.meixiaoliang.portal.modular.system.model.searcher;

import java.io.Serializable;

import lombok.Data;

/**
 * 基本分页查询入参
 *
 * @author meixiaoliang
 * @version $Id: BaseSearcher.java, v 0.1 2018年4月24日 下午1:34:45 meixiaoliang Exp $
 */
@Data
public class BaseSearcher implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2060897652101499491L;

    /** 页码 */
    private long              pageNum          = 1;

    /** 每页展示条数 */
    private long              pageSize         = 10;

}