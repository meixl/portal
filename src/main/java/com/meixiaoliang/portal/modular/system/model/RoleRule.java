
package com.meixiaoliang.portal.modular.system.model;

import lombok.Data;

/**
 * 规则-角色关系
 *
 * @author mei.xiaoliang@qq.com
 */
@Data
public class RoleRule {

    /** 规则 id */
    private Integer ruleId;

    /** 规则类型 */
    private Integer type;

    /** 角色 id */
    private Integer roleId;

}