/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50640
Source Host           : localhost:3306
Source Database       : portal

Target Server Type    : MYSQL
Target Server Version : 50640
File Encoding         : 65001

Date: 2019-02-25 08:24:22
*/

create database portal ;

use portal;


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Records of company
-- ----------------------------

-- ----------------------------
-- Table structure for sys_attach
-- ----------------------------
DROP TABLE IF EXISTS `sys_attach`;
CREATE TABLE `sys_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uuid` varchar(32) NOT NULL COMMENT '文件 uuid',
  `name` varchar(255) NOT NULL COMMENT '附件名称',
  `path` varchar(255) NOT NULL COMMENT '路径',
  `size` double NOT NULL COMMENT '文件大小',
  `suffix` varchar(255) NOT NULL COMMENT '文件尾缀',
  `user_id` smallint(4) DEFAULT NULL COMMENT '上传用户',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` smallint(6) NOT NULL COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '机构名称',
  `parent` smallint(6) NOT NULL COMMENT '上级机构 id',
  `num` tinyint(4) DEFAULT NULL COMMENT '顺序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='人员机构';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '市主管部门', '0', '1');
INSERT INTO `sys_dept` VALUES ('2', '市协会', '0', '2');
INSERT INTO `sys_dept` VALUES ('3', '区县主管部门', '0', '3');
INSERT INTO `sys_dept` VALUES ('4', '越城区', '3', '1');
INSERT INTO `sys_dept` VALUES ('5', '柯桥区', '3', '2');
INSERT INTO `sys_dept` VALUES ('6', '上虞区', '3', '3');
INSERT INTO `sys_dept` VALUES ('7', '新昌县', '3', '4');
INSERT INTO `sys_dept` VALUES ('8', '嵊州市', '3', '5');
INSERT INTO `sys_dept` VALUES ('9', '诸暨市', '3', '6');
INSERT INTO `sys_dept` VALUES ('10', '1111111111', '6', '2');

-- ----------------------------
-- Table structure for sys_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictionary`;
CREATE TABLE `sys_dictionary` (
  `id` int(11) NOT NULL COMMENT '主键',
  `code` smallint(6) DEFAULT NULL COMMENT '字典值',
  `value` varchar(40) DEFAULT NULL COMMENT '描述',
  `type` tinyint(4) DEFAULT NULL COMMENT '类型：1-字典组 2-字典',
  `parent` smallint(6) DEFAULT NULL COMMENT '字典组 id',
  PRIMARY KEY (`id`),
  KEY `idx` (`parent`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dictionary
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL COMMENT '主键id',
  `code` varchar(255) DEFAULT NULL COMMENT '菜单编号',
  `parent` varchar(255) DEFAULT NULL COMMENT '菜单父编号',
  `type` int(1) DEFAULT NULL COMMENT '菜单类型',
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `url` varchar(255) DEFAULT NULL COMMENT 'url地址',
  `num` smallint(6) NOT NULL COMMENT '菜单排序号',
  `status` tinyint(4) DEFAULT NULL COMMENT '菜单状态 :  1:启用   0:不启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '', '0', '1', '我的提醒', '', '', '1', '1');
INSERT INTO `sys_menu` VALUES ('2', '', '1', '2', '我的提醒', '', '/notice/list', '1', '1');
INSERT INTO `sys_menu` VALUES ('3', '', '0', '1', '任务列表', '', '', '2', '1');
INSERT INTO `sys_menu` VALUES ('4', '', '3', '2', '新任务', '', '/task/receiver/pending/list', '1', '1');
INSERT INTO `sys_menu` VALUES ('5', '', '3', '2', '已完成', '', '/task/receiver/done/list', '2', '1');
INSERT INTO `sys_menu` VALUES ('6', '', '3', '2', '已超期', '', '/task/receiver/delay/list', '3', '1');
INSERT INTO `sys_menu` VALUES ('7', '', '3', '2', '我的任务', '', '/task/receiver/list', '4', '1');
INSERT INTO `sys_menu` VALUES ('8', '', '3', '2', '工作计划', '', '/workplan/list', '5', '1');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL COMMENT '角色名称',
  `num` smallint(6) DEFAULT NULL COMMENT '顺序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='系统角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'admin', '系统管理员', '1');
INSERT INTO `sys_role` VALUES ('2', 'role1', '董事长', '1');
INSERT INTO `sys_role` VALUES ('3', 'role2', '经理', '2');
INSERT INTO `sys_role` VALUES ('4', 'role3', '职员', '3');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` tinyint(4) NOT NULL COMMENT '角色 id',
  `menu_id` smallint(6) NOT NULL COMMENT '菜单 id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '用户登录名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `real_name` varchar(50) DEFAULT NULL COMMENT '真实姓名',
  `num` tinyint(4) DEFAULT NULL COMMENT '任务排序',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `status` bit(1) DEFAULT b'0' COMMENT '状态 ( 0: 冻结 1: 启用 )',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `is_deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '617d2e723caad76869fe1e43df25b55b73f273c8', '系统管理员', '0', '', '', null, '\0');
INSERT INTO `sys_user` VALUES ('2', 'eleven', '617d2e723caad76869fe1e43df25b55b73f273c8', '陆总', '1', '', '', null, '\0');

-- ----------------------------
-- Table structure for sys_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_dept`;
CREATE TABLE `sys_user_dept` (
  `user_id` smallint(6) DEFAULT NULL,
  `dept_id` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户-机构部门 关系';

-- ----------------------------
-- Records of sys_user_dept
-- ----------------------------
INSERT INTO `sys_user_dept` VALUES ('1', '0');
INSERT INTO `sys_user_dept` VALUES ('2', '1');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户-角色关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '2');
