# 关于本项目

## 项目介绍
* 本项目为纯后台接口项目，前端采用 vue-element
* 项目主要框架: SpringBoot + MybatisPlus
* 数据库: Mysql + Redis
* 数据库连接池: Druid
* 日志: Logback
* 会话: Shiro
* 文档: swagger2

## 功能介绍
* 用户登录、登出
* 用户-角色、角色-菜单、角色-权限
* 数据字典
* 自动化生成代码 ( controller, pojo, service, serviceImpl, mapper)
* 列表数据快速下载到 excel
* 文件上传、下载

## 如何启动 ( 文件在哪请自己找 )
* 数据库初始化。运行 init.sql 初始化数据库
* 修改 application-dev.properties 中 Mysql、Redis 链接。
* 用户初始化。修改 application-dev.properties 中的 password.salt ( 盐 )、password.transTime ( 加密次数 )。运行 UserInit.java 初始化用户密码。
* 运行 StartApplication 启动项目。
* 登录接口：LoginController.java 中自行请求。

## 联系方式
微信：meixiaoliang001。欢迎指导。
